using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "ScriptableObjects/Card")]
public class Card : ScriptableObject
{
    public new string name;
    [TextArea(3,10)]
    public string description;

    public Sprite back;
    public Sprite front;
}
