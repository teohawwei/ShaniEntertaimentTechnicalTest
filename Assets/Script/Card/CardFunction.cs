using UnityEngine;
using UnityEngine.UI;

public class CardFunction : MonoBehaviour
{
    #region Class Member
    public Card card;

    public bool isFlip;
    public Image image;
    public Text nameText;
    public Text descriptionText;
    #endregion


    #region Unity Function
    void Start()
    {
        isFlip = false;
        image.sprite = card.back;
        nameText.text = card.name;
        descriptionText.text = card.description;

        nameText.gameObject.SetActive(false);
        descriptionText.gameObject.SetActive(false);
    }
    #endregion


    #region Public Function
    public void FlipCard()
    {
        isFlip = true;
        image.sprite = card.front;

        nameText.gameObject.SetActive(true);
        descriptionText.gameObject.SetActive(true);
    }
    #endregion
}
