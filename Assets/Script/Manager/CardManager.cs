using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : MonoBehaviour
{
    #region Class Member
    public List<CardFunction> cardEntity;

    [SerializeField]
    private bool disableFlipCard = false;
    #endregion


    #region Unity Function
    private void Update()
    {
        foreach(CardFunction card in cardEntity)
        {
            if (card.isFlip == true)
            {
                disableFlipCard = true;
            }
        }

        if(disableFlipCard == true)
        {
            foreach (CardFunction card in cardEntity)
            {
                Button button = card.GetComponent<Button>();
                button.enabled = false;
                card.enabled = false;
            }
        }
    }
    #endregion
}
